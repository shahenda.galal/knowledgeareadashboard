$(document).ready(function () {
if( $('html').attr('dir') === 'rtl' ){
    $('body').addClass('rtl')
} else{
    $('body').addClass('ltr')

}
if( $('select').length > 0 ){
    $('select').niceSelect();
} 
if( $('.datepicker').length > 0 ){
    $('.datepicker').datepicker();
} 
if( $('#copyField').length > 0 ){
    $('#copyToButton').click(function(){
        /* Get the text field */
        var copyText = document.getElementById("copyField");
        
        /* Select the text field */
        copyText.select();
        copyText.setSelectionRange(0, 99999); /*For mobile devices*/
        
        /* Copy the text inside the text field */
        document.execCommand("copy");
    })
} 
if( $('.new-organization-field').length > 0 ){
    $('.new-organization-field').hide();
    $('.domain').prop('disabled', 'disabled')            
    $('#organizationName').on('change', function(){
        if( $('#organizationName option:selected').val() === 'other' ){
            $('.new-organization-field').show();            
        }else{
            $('.new-organization-field').hide();

        }
    });
    $('#organizationType').on('change', function(){
        if( $('#organizationType option:selected').val() === 'other' ){
            $('.domain').prop('disabled', '')            
        }else{
            $('.domain').prop('disabled', 'disabled')            

        }
    });
} 
if( $('.custom-file-input').length > 0 ){
    var mainLabel
    $('.custom-file-input').each(function(){

        $(this).siblings('.custom-file-label').attr('data-label',$(this).siblings('.custom-file-label').html() );
        $(this).on('change', function(){
            mainLabel =   $(this).siblings('.custom-file-label').attr('data-label')
            var fullPath = $(this).val();
            if (fullPath) {
                var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
                var filename = fullPath.substring(startIndex);
                if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                    filename = filename.substring(1);
                }
                $(this).siblings('.custom-file-label').html(filename);
            }else{
                $(this).siblings('.custom-file-label').html(mainLabel);
            }
        });
    });
}         $('.toggle-sidebar').one('click', function(){
    $('.search-form').insertBefore($('.menu-items'));
});

(function($) {

    'use strict';

    // Initialize a basic dataTable with row selection option
    var initBasicTable = function() {

        var table = $('#basicTable');

        var settings = {
            "sDom": "t",
            "destroy": true,
            "paging": false,
            "scrollCollapse": true,
            "aoColumnDefs": [{
                'bSortable': false,
                'aTargets': [0,1,4,5]
            }],
            "order": [

            ]

        };

        table.dataTable(settings);

        $('#basicTable input[type=checkbox]').click(function() {
            if ($(this).is(':checked')) {
                $(this).closest('tr').addClass('selected');
            } else {
                $(this).closest('tr').removeClass('selected');
            }

        });

    }
    if( $('#basicTable').length > 0 ){
        initBasicTable();
    }

})(window.jQuery);
});
$(window).resize(function () { 
    if($(window).width() > 991 ){
        $('.search-form').appendTo($('.header .col-sm-9'));
    }
    else{
        $('.toggle-sidebar').one('click', function(){
            $('.search-form').insertBefore($('.menu-items'));
        });
    }
});